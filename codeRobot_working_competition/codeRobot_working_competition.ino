// height of line sensor < 3.8 mm

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>

// servo
Servo platform_servo;

int servo_pos = 0;

//initalise motorshield
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// initalise motors
Adafruit_DCMotor *leftMotor = AFMS.getMotor(4);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(3);
Adafruit_DCMotor *grabberMotor = AFMS.getMotor(2);

//initalise motor variables
int motorSpeed = 255;

// define turning times
// needs calibration
// int timeLengthTurn90 = 1000;
int timeLength = 200;    // calibration value (value below 250 works well) // do not change

// define cross counting number
int cross_no = 0;

//define pins for line sensors
#define leftLineSensor A0
#define centreLineSensor A1
#define rightLineSensor A2

// doesn't matter
float leftLineValue = 0;
float centreLineValue = 0 ;
float rightLineValue = 0;

float leftLineValueTolerance = 200; 
float centreLineValueTolerance = 200; 
float rightLineValueTolerance = 200;

// dm
bool isOnCross = false;


// initialize push button

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

// define button low
int read_button_HL_state = 0;

//initalise Led
#define plasticLed 3 // green (non-metal)
#define metalLed 4 // red (metal)
#define crossLed 8
#define amberLED 11

// define my_switch
#define my_switch 6

// define block
int block_identity = 1; // red/ steel is 1
int block_no = 1; 

// define return of cross 
int detect_cross;

// define return of grabbing process:
int grabbing_success;

// define cross() time
int cross (int cross_time = 250);

// define turning time
void turn_stationery_180 (int turn_stationery_180_time = 3200);
void turn_stationery_right (int turn_stationery_right_time = 1650); // needs calibration
void turn_stationery_left (int turn_stationery_left_time = 1650); // needs calibration

//initalise metal detector
const byte npulse = 25;
const bool debug = true;
const byte pin_pulse=10;
const byte pin_cap  =A3;
// const byte pin_LED1 =12; //yellow
// const byte pin_LED2 =11; // green
const byte pin_tone =10;
const int nmeas=500;  //measurements to take
long int sumsum=0; //running sum of 64 sums 
long int skip=0;   //number of skipped sums
long int diff=0;        //difference between sum and avgsum
long int flash_period=0;//period (in ms) 
long unsigned int prev_flash=0; //time stamp of previous flash
bool contact_open = false;

// initialize
float average;

void setup() {

  // setup push button
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  //setup motor
  AFMS.begin();
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
  grabberMotor->setSpeed(0);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  grabberMotor->run(FORWARD);
  delay(1000);

  // LED
  pinMode(crossLed, OUTPUT);
  pinMode(plasticLed, OUTPUT);
  pinMode(metalLed, OUTPUT);
  pinMode(amberLED, OUTPUT);

  // swtich
  pinMode(my_switch, INPUT);

  // initialize LED set amberLED HIGH
  digitalWrite(amberLED, LOW);

  // initialize servo
  platform_servo.attach(9);

  // servo raised up initially
  servo_pos = 10;
  platform_servo.write(servo_pos);              // tell servo to go to position in variable 'pos'

  Serial.begin(9600); // set up serial monitor
}

void loop() {

  // push button
  read_button_HL_state = digitalRead(buttonPin); // push button, then read_button_HL_state goes as HIGH and initial buttonState = 0
  
  if (read_button_HL_state == HIGH && buttonState == 0){ 
    buttonState = 1; // goes from 0 to 1
    digitalWrite(amberLED, HIGH);

  
    // the loop after pressing the button 
    while (buttonState == 1){

      line_sensor_readings(); // read line sensor and display values

      cross(); // test whether at cross
      if (cross_no == 3){
        picking_first_block(); // pick up + turn around

        while(true){
          line_sensor_readings();
          detect_cross = cross();
          
          // if meets a cross () (if have problem, go to here first)
          if (detect_cross == 1){
            dropping_block_route(block_identity, block_no, cross_no); // arriving at collection area, then go to the desinated area of dropping, then go back to main track, and start loop 2
            
            // finishes 1st block, start of second block
            while (true) {

              // intentionally separate out block one and the others so that no need for that many if else statements

              // collection loop 
              while (true){
                line_sensor_readings();
                
                detect_cross = cross();

                // if detects a cross, and cross_no += 1 in cross() function
                if (detect_cross == 1) {
                  // the cross at collection area
                  detection_route_algorithm(block_no); // for block_no >= 2
                  break; // break and go to another while loop
                }
                straight_line_algorithm(timeLength);
                stop_button_check();
              } // end of 1st inner loop

              // dropping 
              while (true) {
                line_sensor_readings();
                
                // if detects a cross, and cross_no += 1 in cross() function
                detect_cross = cross();
                if (detect_cross == 1){
                  stopMoving(1000); // testing stop
                  dropping_block_route(block_identity, block_no, cross_no); // drop and back on track
                  break; // back to first while loop
                }
                straight_line_algorithm(timeLength);
                stop_button_check();
              } // end of 2nd inner while loop          
            } // end of outer while loop
          }
          straight_line_algorithm(timeLength/2); 
          stop_button_check();
        }
      } // end button while loop
      straight_line_algorithm(timeLength/2);
    } // end if statement (no longer 1)
  } // end loop
}


// DECLARE FUNCTIONS TO MOVE --

// read line sensor and display values
void line_sensor_readings(){
  // line sensor keeps reading
  leftLineValue = analogRead(leftLineSensor);
  rightLineValue = analogRead(rightLineSensor);
  centreLineValue = analogRead(centreLineSensor);

  Serial.println("LOOP DATA:");
  Serial.println(leftLineValue);
  Serial.println(rightLineValue);
  Serial.println(centreLineValue);
}

void moving_flash_led(int x){
  int loops = x / 1000;
  int i = 0;
  for (i; i < loops; i++) {
    digitalWrite(amberLED, LOW);
    delay(500);
    digitalWrite(amberLED, HIGH);
    delay(500);
  }
  int excessDelay = x - loops * 1000;
  delay(excessDelay);
  digitalWrite(amberLED, LOW);
}

void stop_button_check(){

  read_button_HL_state = digitalRead(buttonPin); // pushes button, then read_button_HL_state goes to HIGH and initial buttonState =0)

  if (read_button_HL_state == HIGH && buttonState == 1){ 
    buttonState = 0;

    // set amberLED LOW
    digitalWrite(amberLED, LOW);
    delay(100);
    stopMoving(999999999999); // need to change this as well
    exit(0);
    // need a delay
  }
}

void moveForwards (int x) {
  digitalWrite(amberLED, HIGH);
  leftMotor->setSpeed(motorSpeed);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  delay(x);
}

void moveBackwards (int x) {
  leftMotor->setSpeed(motorSpeed);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(BACKWARD);
  delay(x);
}

void stopMoving (int x) {
  leftMotor->setSpeed(0);
  rightMotor->setSpeed(0);
  digitalWrite(amberLED, LOW);
  delay(x);
}

// this is moving turn
void turnRight (int x) {
  leftMotor->setSpeed(motorSpeed);
  rightMotor->setSpeed(70);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  delay(x);
}

// this is moving turn
void turnLeft (int x) {
  leftMotor->setSpeed(70);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  delay(x);
}


// this is stationery turn
// stationery means after turning, the centre of the car should still be in the same location
void turn_stationery_180 (int x) {
  digitalWrite(amberLED, HIGH);
  leftMotor->setSpeed(motorSpeed);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  delay(x);
  stopMoving(10);
  
}

// needs calibration
// stationery turn right
// stationery means after turning, the centre of the car should still be in the same location
void  turn_stationery_right (int x) {
  digitalWrite(amberLED, HIGH);
  leftMotor->setSpeed(motorSpeed);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  delay(x);
  stopMoving(10);
}

// needs calibration
// stationery turn left
// stationery means after turning, the centre of the car should still be in the same location 
void turn_stationery_left (int x) {
  digitalWrite(amberLED, HIGH);
  leftMotor->setSpeed(motorSpeed);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  delay(x);
  stopMoving(10);
}

// needs calibration
// stationery turn right
// stationery means after turning, the centre of the car should still be in the same location
void  turn_stationery_moving_right (int x) {
  digitalWrite(amberLED, HIGH);
  leftMotor->setSpeed(motorSpeed * 0.5);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  delay(x);
  stopMoving(10);
}

// needs calibration
// stationery turn left
// stationery means after turning, the centre of the car should still be in the same location 
void turn_stationery_moving_left (int x) {
  digitalWrite(amberLED, HIGH);
  leftMotor->setSpeed(motorSpeed * 0.5);
  rightMotor->setSpeed(motorSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  delay(x);
  stopMoving(10);
}

void straight_line_algorithm (int timeLength){

      

      // below a value, detects a line (if cannot detect, then above above value)
      // if the value detected is greater than 800, we know the left sensor does not detect the line
      // if the value detected is smaller than 800, then we know the left sensor detects the line
      // if we raise the value from 800 to 900, then the left sensor is more sensitive in detecting the line (more chance to detect a line)
      // in general, if we raise the tolerance value, it is easier for the line sensor to reach a value below the tolerance, hence easier to trigger turn direction function

      // too right
      if (leftLineValue < leftLineValueTolerance && rightLineValue > rightLineValueTolerance) {
        turnLeft(timeLength); // turn left
        }
        
      // too left
      else if (rightLineValue < rightLineValueTolerance && leftLineValue > leftLineValueTolerance){
      turnRight(timeLength); // turn right
      }
      
      // just centre
      else if (leftLineValue > leftLineValueTolerance && rightLineValue > rightLineValueTolerance) {
        moveForwards(timeLength); // nothing happens
      }
      else {
        moveForwards(timeLength);
    }
  
}

// detects whether there is a cross
int cross (int x) {
  if (leftLineValue < leftLineValueTolerance && rightLineValue < rightLineValueTolerance && centreLineValue < centreLineValueTolerance) {
  isOnCross = true;
  digitalWrite(crossLed, HIGH);
  delay(x);
  digitalWrite(crossLed, LOW);
  cross_no += 1;

  return 1;
  }
  return 0;
}

void picking_first_block(){
  
  
  stopMoving(100);
  moveBackwards(1000); // callibration
  stopMoving(50);
  servo_down();

  
  //calibration (detecting air)

  float sum = 0;
  for(int i; i < 30; i++){
  diff = findDiff();
  Serial.println(diff);
  sum += diff;
  }
  Serial.println("-------");
  float average = sum / 30; // changed from 30 to 50
  Serial.print("Calibration: ");
  Serial.println(average);

  moveForwards(1000); // callibration
  stopMoving(50);
  grabber_motor_close();
  
  // detectMetal();

  //detecting the block

  float sum2 = 0;
  for(int i; i < 30; i++){
    diff = findDiff();
    Serial.println(diff);
    sum2 += diff;
  }
  float metalAverage = sum2 / 30 ;
  Serial.println("-------");
  Serial.print("Block Average: ");
  Serial.println(metalAverage);
  float value = metalAverage - average;
  Serial.println("-------");
  Serial.print("Value: ");
  Serial.println(value);
  if(value > -40){
    Serial.println("Plastic detected");
    block_identity = 0;
    digitalWrite(plasticLed, HIGH);
    delay(5000);
    } else{
      Serial.println("Metal detected");
      block_identity = 1;

      digitalWrite(metalLed, HIGH);
      Serial.println("Block detected");
      delay(5000);
    }
  
  servo_up();
  digitalWrite(amberLED, HIGH);
  turn_stationery_180(3800);  // timeLength = 200 // turn 180)
}

void dropping_block_route (int block_identity, int block_no, int cross_no) {

  stopMoving (1000); // callibration 
  moveForwards ((block_no - 1) * 100); // callibrate this

  if (block_identity == 1){ //Metal block, red area // need to define block
    turn_stationery_right();
  } 
  else if (block_identity == 0){
    turn_stationery_left();
  }

  //dropping process code 
  dropping_process();
    
  // going out of collection area
  
  if (block_identity == 1 && block_no != 5){ //Metal block, red area // need to define block
    digitalWrite(metalLed, LOW);
    turn_stationery_right(2300);
  } 
  else if (block_identity == 0 && block_no != 5){
    digitalWrite(plasticLed, LOW);
    turn_stationery_left(2300);
  }

  // else last block (4 is chosen now) and red
  else if (block_identity == 1 && block_no == 5) {
    digitalWrite(metalLed, LOW);
    turn_stationery_right();
    back_to_start();
  }

  // else last block (4 is chosen now) and blue
  else if  (block_identity == 0 && block_no == 5) {
    turn_stationery_left();
    digitalWrite(plasticLed, LOW);
    back_to_start();
  }


  cross_no = 2;
  block_no += 1; // calibratoin needed
}

// needs calibration
void detection_route_algorithm(int block_no){
  // detects cross
  stopMoving(50);
  moveBackwards(1300);
  stopMoving(100);
  servo_down();
  turn_stationery_right();
  moveForwards(800);

  stopMoving(100);
  turn_stationery_left();
  moveForwards(2800);
  stopMoving(100);
  grabber_motor_close();
  moveBackwards(2800);
  stopMoving(100);
  turn_stationery_left();
  moveForwards(2000);
  stopMoving(100);

  // go to the block
  turn_stationery_right(2000);
  grabber_motor_open();
  moveForwards(1900);
  stopMoving(100);
  moveBackwards(1000);
  stopMoving(100);

  average = metalCalibration();
  stopMoving(1000);
  moveForwards(1000);
  stopMoving(100);
  grabber_motor_close();
  stopMoving(50);

  
  testBlockForMetal(average);
  stopMoving(1000);

  moveBackwards(2700);
  stopMoving(100);
  servo_up();
  turn_stationery_left();
  stopMoving(50);
  moveBackwards(2000);

  while (true)
  {
    line_sensor_readings();
    turn_stationery_moving_left(100); // needs calibration
    if (leftLineValue < leftLineValueTolerance){
      break;
    }
  }
}




  
  // // detection loop begins, just in case 1st 2 loops do not detect anything
  // while (true) {
  //   stopMoving(100);
  //   turn_stationery_left();
  //   moveForwards(3000); // towards the block
  //   stopMoving(100);

  //   // grabbing mechanism
  //   grabbing_success = grabbing_process();

  //   moveBackwards(2500);
  //   // 1 means collected block, can start return journey
  //   if (grabbing_success == 1) {
      
  //     while (true){
  //       line_sensor_readings();
  //       turn_stationery_moving_right(100); // needs calibration
  //       if (rightLineValue < rightLineValueTolerance){
  //         break;
  //       }
  //     }
  //     break; // breaks the loop
  //   } else { 
  //     // not detect block // requires serious calibration
  //     moveBackwards(1500);
  //     stopMoving(50);
  //     turn_stationery_left();
  //     moveForwards(1000);
  //     stopMoving(50);
  //     turn_stationery_right();
  //     moveForwards(3000);
  //     stopMoving(50);
  //     grabbing_process();

  //     // if grab and detected
  //      if (grabbing_process() == 1){
  //         while (true){
  //           line_sensor_readings();
  //           turn_stationery_moving_left(100); // needs calibration
  //         if (leftLineValue < leftLineValueTolerance){
  //           break;
  //           }
  //         }
  //      } else {
  //       moveBackwards(1500);
  //       stopMoving(100);
  //       turn_stationery_right();
  //       moveForwards(600);
  //     }
  //   }
  // }  


void grabber_motor_close () { 
  grabberMotor-> setSpeed(255);
  grabberMotor -> run(FORWARD);
  delay(4000);
  grabberMotor-> setSpeed(50);
  delay(50);
}

void grabber_motor_open () {
  grabberMotor-> setSpeed(255);
  grabberMotor -> run(BACKWARD);
  delay(3000);
  grabberMotor-> setSpeed(0);
}

void servo_down () {
  

   for (servo_pos = 0; servo_pos <= 120; servo_pos += 2) { // goes from 0 degrees to 180 degrees
   // in steps of 1 degree
   platform_servo.write(servo_pos);              // tell servo to go to position in variable 'pos'
   delay(50);                       // waits 15ms for the servo to reach the position
   }
}

void servo_up () {

 
 for (servo_pos = 120; servo_pos >= 0; servo_pos -= 1) { // goes from 0 degrees to 180 degrees
   // in steps of 1 degree // minus 1
   platform_servo.write(servo_pos);              // tell servo to go to position in variable 'pos'
   delay(50);                       // waits 15ms for the servo to reach the position
 } 
}

int grabbing_process() {
  servo_down();
  grabber_motor_close();
  delay(200);
  digitalRead(my_switch);
  if (my_switch == 1){
    // metal detection mechanism
    float sum = 0;
    for(int i; i < 30; i++){
      diff = findDiff();
      Serial.println(diff);
      sum += diff;
    }
    float average = sum / 30; // changed from 30 to 50

    float value = findDiff() - average;
    Serial.println(value);
    if(value > -40){
      Serial.println("Plastic detected");
      block_identity = 0;

      digitalWrite(plasticLed, HIGH);
      Serial.println("Block detected");
    }
    else{
      Serial.println("Metal detected");
      block_identity = 1;

      digitalWrite(metalLed, HIGH);
      Serial.println("Block detected");
    }
    servo_up();
    return 1;
  }
  else {
    grabber_motor_open();
    return 0;
  }
}

void dropping_process() {
  grabber_motor_open();
}

// always return 1 for now
// whether my_switch detects
int collection_success (){

}

void back_to_start(){
  line_sensor_readings();
  if (cross){
    moveForwards(500); // calibration
    stopMoving(99999999999999999999);
  }
  straight_line_algorithm(timeLength);
}

void detectMetal(){
  float sum = 0;
  for(int i; i < 30; i++){
    diff = findDiff();
    Serial.println(diff);
    sum += diff;
  }
  float average = sum / 30; // changed from 30 to 50

  float value = findDiff() - average;
  Serial.println(value);
  if(value > -40){
    Serial.println("Plastic detected");
    block_identity = 0;

    // digitalWrite(metalLed, HIGH);
    // delay(1000);
    // Serial.println("Block detected");
    // digitalWrite(metalLed, LOW);
    // delay(1000);

  }
  else{
    Serial.println("Metal detected");
    block_identity = 1;

    // digitalWrite(metalLed, HIGH);
    // delay(1000);
    // Serial.println("Block detected");
    // digitalWrite(metalLed, LOW);
    // delay(1000);
  }

}

float findDiff() {
  int minval=1023;
  int maxval=0;
  
  //perform measurement
  long unsigned int sum=0;
  for (int imeas=0; imeas<nmeas+2; imeas++){
    //reset the capacitor
    pinMode(pin_cap,OUTPUT);
    digitalWrite(pin_cap,LOW);
    delayMicroseconds(20);
    pinMode(pin_cap,INPUT);
    //apply pulses
    for (int ipulse = 0; ipulse < npulse; ipulse++) {
      digitalWrite(pin_pulse, HIGH); //takes 3.5 microseconds
      delayMicroseconds(3);
      digitalWrite(pin_pulse, LOW);  //takes 3.5 microseconds
      delayMicroseconds(3);
    }
    //read the charge on the capacitor
    int val = analogRead(pin_cap); //takes 13x8=104 microseconds
    minval = min(val,minval);
    maxval = max(val,maxval);
    sum+=val;
  }

  //subtract minimum and maximum value to remove spikes
  sum-=minval; sum-=maxval;
  
  //process
  if (sumsum==0) sumsum=sum<<6; //set sumsum to expected value
  long int avgsum=(sumsum+32)>>6; 
  diff=sum-avgsum;
  if (abs(diff)<avgsum>>10){      //adjust for small changes
    sumsum=sumsum+sum-avgsum;
    skip=0;
  } else {
    skip++;
  }
  if (skip>64){     // break off in case of prolonged skipping
    sumsum=sum<<6;
    skip=0;
  }  
    
  if (debug){
//    Serial.print(nmeas); 
//    Serial.print(" ");
//    Serial.print(minval); 
//    Serial.print(" ");
//    Serial.print(maxval); 
//    Serial.print(" ");
//    Serial.print(sum); 
//    Serial.print(" ");
//    Serial.print(avgsum); 
//    Serial.print(" ");
//    Serial.print(diff); 
//    Serial.print(" ");
//    Serial.print(flash_period); 
//    Serial.println();
  }
  return diff;
}

float metalCalibration() {
  float sum = 0;
  for(int i; i < 30; i++){
  diff = findDiff();
  Serial.println(diff);
  sum += diff;
  }
  Serial.println("-------");
  float average = sum / 30; // changed from 30 to 50
  Serial.print("Calibration: ");
  Serial.println(average);
  return average;
}
float testBlockForMetal(float average) {
    float sum2 = 0;
  for(int i; i < 30; i++){
    diff = findDiff();
    Serial.println(diff);
    sum2 += diff;
  }
  float metalAverage = sum2 / 30 ;
  Serial.println("-------");
  Serial.print("Block Average: ");
  Serial.println(metalAverage);
  float value = metalAverage - average;
  Serial.println("-------");
  Serial.print("Value: ");
  Serial.println(value);
  if(value > -40){
    Serial.println("Plastic detected");
    block_identity = 0;
    digitalWrite(plasticLed, HIGH);
} else{
    Serial.print("Metal detected");
    block_identity = 1;
    digitalWrite(metalLed, HIGH);

  
}
}
