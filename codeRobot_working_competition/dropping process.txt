// check whether line sensor can sense red line 

dropping process general(variable blocktype, block number )
    if block type == red:
        turn right // have to decide stationery turn or moving turn
    else turn left
    move forward (n timelength depending on block number)
    
    // dropping process
    lower arm
    open arm
    up arm // change this later
    
    go back (m timelength depending on block number) // or the one below, reaches line
    go back (like how it goes in) until senses cross
    if block type == red:
        turn right // have to decide stationery turn or moving turn
    else turn left
    
    turn left/right (facing the collection area)
    set cross_no = 2
    rerun the original line sensing code (loop 3 starts)


detection_route:
    reaches cross
    keep pushing for some time
    go backwards
    stop
    turn stationery right 
    move forward

    while True:
 
        stop // need calibration
        turn stationery left
        move forward for some time

        grabbing_mechanism()

        
        if grab and detected
            go to centre line
            break the loop

        // not detected, go to the other side
        if not detected
            go backwards // calibration
            stop
            turn stationery left
            move forward // need calibration
            stop
            turn stationery right
            grabbing_mechanism()

            if grab and detected
                bring it back                go to centre line
                break the loop

            if no
                go backwards
                stop
                turn stationery right
                move forward
                



loop
    while true
        line sensor algorithm
        if detect cross
            if cross_no == 3
                pick_up_1 algorithm            
                break
        

    turn around to go back

    while true
        // going back
        while true
            line sensor algorithm
            if detect cross
                if cross_no == 4
                    dropping route algorithm
                    cross_no = 2
                    block_no += 1
                    break

        // intentionally separate out block one and the others so that no need for that many if else statements
        while true
                line sensor algorithm
                if detect cross
                    if cross_no == 3
                        picking route algorithm // for block_no > 1            
                        break    
        
        turn around to go back










    





    